import React, { Component } from 'react';
import api from '../../services/api';
import './styles.css';

import { Link } from 'react-router-dom';

export default class Pokemon extends Component {
    state = {
        name : [],
        sprites: [],
        moves: [],
        abilities: [],
    }

    async componentDidMount() {
        const { nome } = this.props.match.params;

        const poke = await api.get(`/pokemon/${nome}`);

        console.log(poke.data);

        const { name, sprites, moves, abilities } = poke.data;
    
        this.setState({ name, sprites, moves, abilities });
    }

    render() {
        const { name, sprites, moves, abilities } = this.state;
        
        return(
            <div className="poke-detalhe">

               <div className="perfil">
                    <h1>{ name }</h1>
                    <img src={sprites.front_default} />
               </div>

               <div className="habilidades">
                    <h1>Habilidades</h1>
                    <br />
                    {abilities.map((ability, index) => (
                        <article key={index}>
                            <ul>
                                <li><h4>{ability.ability.name}</h4></li>
                            </ul>
                        </article>
                    ))}
               </div>

               <div className="movimentos">
                    <h1>Movimentos</h1>
                    <hr />
                    <div className="lista">
                        {moves.map((move, index) => (
                            <article key={index} id='movimentos'>    
                                <strong>{move.move.name},</strong>                           
                            </article>    
                        ))}
                    </div>
                </div>
                        
                <Link to={`/`}>Voltar</Link>
			    
            </div>
        );
    };
}
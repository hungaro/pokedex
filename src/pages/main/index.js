import React, { Component } from 'react';
import api from '../../services/api';
import './styles.css';

import { Link } from 'react-router-dom';

export default class Main extends Component {
    state = {
        pokemons: [],   
    }

    componentDidMount() {
        this.loadPokemon();
    }

    loadPokemon = async () => {
        const response = await api.get('/pokemon/?offset=0&limit=964');

        const { results, next, previous } = response.data;
        
        console.log(response.data);

        this.setState({ pokemons: results, next, previous });
    };

    
    
    render() {
        const { pokemons } = this.state;
        
        return (
            <div className="pokemon-lista">
                {pokemons.map((results, index) => (
                    <article key={index}>
                        <h2><strong>{results.name}</strong></h2>
                        <Link to={`/poke/${results.name}`}>Detalhes</Link>
                    </article>
                ))}
            </div>
            

        )
    }
}